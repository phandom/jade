package agent;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

import Behaviour.RolloDownBehaviour;
import Behaviour.RolloUpBehaviour;
import Behaviour.StartSever;
import Behaviour.TimerOffBehaviour;
import Behaviour.TimerOnBehaviour;
import Protocols.FindTimeToSettDown;
import Protocols.FindTimeToSettOff;
import Protocols.FindTimeToSettOn;
import Protocols.FindTimeToSettUp;
import Protocols.FindUserAgentTellStatusOffBehaviour;
import Protocols.FindUserAgentTellStatusOnBehaviour;
import Protocols.JalousieFindUserAgentTellStatusOffBehaviour;
import Protocols.JalousieFindUserAgentTellStatusOnBehaviour;
import Protocols.LightOffFromTimer;
import Protocols.LightOnFromTimer;
import Protocols.RolloDownFromTimer;
import Protocols.RolloUpFromTimer;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;
import jade.util.leap.Serializable;
import smartHOntology.Concept_Status;
import smartHOntology.Concept_Time_ToSet;
import smartHOntology.Jalousie;
import smartHOntology.Light;
import smartHOntology.RoomOntology;

public class DeviceAgent extends Agent implements Serializable {

	/**
	 * athor 
	 * LightAgent hat die Aufgabe die Geräte zu verwalten.
	 * @author  Phan Anh Tran
	 */
	
	
	/**
	 * message language FIPA-SL
	 */
	private Codec codec = new SLCodec();

	/**
	 * ontology used for semantic parsing
	 */
	private Ontology ontology = RoomOntology.getInstance();

	/**
	 * for serializing
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * agent initializing
	 */
	// transient protected Gui myGui;

	private AID userAgent;

	public AID getRoomAgentAID() {
		return userAgent;
	}

	public void setUserAgentAID(AID useragent2) {
		userAgent = useragent2;
	}

	@Override
	public void setup() {

		super.setup();

		this.getContentManager().registerLanguage(this.codec);
		this.getContentManager().registerOntology(ontology);

		// Register Lights with DF
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(this.getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("Light-Status");
		sd.setName(this.getLocalName() + "-Light-Status");
		// add Ontologies, Languages, Interaction Protocols here
		dfd.addServices(sd);
		try {
			DFService.register(this, dfd);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}

		Concept_Time_ToSet time = new Concept_Time_ToSet();

		// fake Devices werden hier erstellt. (light1, light2, rollo1, roll2)
		
		Concept_Status down = new Concept_Status("down");
		Concept_Status up = new Concept_Status("up");
		Concept_Status on = new Concept_Status("on");
		Concept_Status off = new Concept_Status("off");
		Light light1 = new Light();
		light1.setDevice("01");
		light1.setStatus(on);
		light1.setlocation("Room101");

		Light light2 = new Light();
		light2.setDevice("02");
		light2.setStatus(off);
		light2.setlocation("Room102");

		Jalousie rollo1 = new Jalousie();
		rollo1.setDevice("01");
		rollo1.setStatus(down);
		rollo1.setlocation("Room101");

		Jalousie rollo2 = new Jalousie();
		rollo2.setDevice("02");
		rollo2.setStatus(up);
		rollo2.setlocation("Room102");

		// this.addBehaviour( new ServeLightStatus());

		MessageTemplate mt = AchieveREResponder.createMessageTemplate(FIPANames.InteractionProtocol.FIPA_REQUEST);
		this.addBehaviour(new AchieveREResponder(this, mt) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
				// TODO Auto-generated method stub
				ACLMessage response = request.createReply();
				try {
				} catch (Exception e) {
					response.setPerformative(ACLMessage.NOT_UNDERSTOOD);
					response.setContent("Wrong Room or Device");
					throw new NotUnderstoodException(response);
				}
				response.setPerformative(ACLMessage.AGREE);
				return response;
			}

			@Override
			protected ACLMessage prepareResultNotification(ACLMessage request, ACLMessage response)
					throws FailureException {

				if (request.getContent().equals("Status?")) {
					try {

						if (response == null) {
							response = request.createReply();
						}
						response.setPerformative(ACLMessage.INFORM);

						this.myAgent.send(response);

					} catch (Exception e) {
						throw new FailureException(response);
					}
					return response;

				} else if (request.getContent().equals("light2")) {
					try {
						String status = new String(request.getContent());
						if (response == null) {
							response = request.createReply();
						}
						response.setPerformative(ACLMessage.INFORM);
						response.setContent("light2" + light2.getStatus().getStatus());
						if (light2.getStatus().getStatus().equals("on")) {
							this.myAgent.addBehaviour(new FindUserAgentTellStatusOnBehaviour(this.getAgent()));
						}
						if (light2.getStatus().getStatus().equals("off")) {
							this.myAgent.addBehaviour(new FindUserAgentTellStatusOffBehaviour(this.getAgent()));
						}
						this.myAgent.send(response);
					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;
				}
				
				else if (request.getContent().equals("jalousie2")) {
					try {
						String status = new String(request.getContent());
						if (response == null) {
							response = request.createReply();
						}
						response.setPerformative(ACLMessage.INFORM);
						response.setContent("jalousie2" + rollo2.getStatus().getStatus());
						if (rollo2.getStatus().getStatus().equals("down")) {
							this.myAgent.addBehaviour(new JalousieFindUserAgentTellStatusOnBehaviour(this.getAgent()));
						}
						if (rollo2.getStatus().getStatus().equals("up")) {
							this.myAgent.addBehaviour(new JalousieFindUserAgentTellStatusOffBehaviour(this.getAgent()));
						}
						this.myAgent.send(response);
					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;
				}

				else if (request.getContent().equals("jalousie1")) {
					try {
						String status = new String(request.getContent());
						if (response == null) {
							response = request.createReply();
						}
						response.setPerformative(ACLMessage.INFORM);
						response.setContent("jalousie1" + rollo1.getStatus().getStatus());
						if (rollo1.getStatus().getStatus().equals("up")) {
							this.myAgent.addBehaviour(new JalousieFindUserAgentTellStatusOffBehaviour(this.getAgent()));
						}
						if (rollo1.getStatus().getStatus().equals("down")) {
							this.myAgent.addBehaviour(new JalousieFindUserAgentTellStatusOnBehaviour(this.getAgent()));
						}
						this.myAgent.send(response);
					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;
				}


				else if (request.getContent().equals("light1")) {
					try {
						String status = new String(request.getContent());
						if (response == null) {
							response = request.createReply();
						}
						response.setPerformative(ACLMessage.INFORM);
						response.setContent("light1" + light1.getStatus().getStatus());
						if (light1.getStatus().getStatus().equals("on")) {
							this.myAgent.addBehaviour(new FindUserAgentTellStatusOnBehaviour(this.getAgent()));

						}
						if (light1.getStatus().getStatus().equals("off")) {
							this.myAgent.addBehaviour(new FindUserAgentTellStatusOffBehaviour(this.getAgent()));
						}

						this.myAgent.send(response);

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;

				}

				else if (request.getContent().equals("SETOFF")) {
					try {
						String status = new String(request.getContent());
						if (response == null) {
							response = request.createReply();
						}
						response.setPerformative(ACLMessage.INFORM);
						response.setContent("will ask for time!");

						this.myAgent.addBehaviour(new FindTimeToSettOff(this.getAgent()));

						this.myAgent.send(response);

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;

				}

				else if (request.getContent().equals("SETON")) {
					try {
						String status = new String(request.getContent());
						if (response == null) {
							response = request.createReply();
						}
						response.setPerformative(ACLMessage.INFORM);
						response.setContent("will ask for time!");

						this.myAgent.addBehaviour(new FindTimeToSettOn(this.getAgent()));

						this.myAgent.send(response);

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;

				}

				else if (request.getContent().equals("SETUP")) {
					try {
						String status = new String(request.getContent());
						if (response == null) {
							response = request.createReply();
						}
						response.setPerformative(ACLMessage.INFORM);
						response.setContent("will ask for time!");

						this.myAgent.addBehaviour(new FindTimeToSettUp(this.getAgent()));

						this.myAgent.send(response);

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;

				}

				else if (request.getContent().equals("SETDOWN")) {
					try {
						String status = new String(request.getContent());
						if (response == null) {
							response = request.createReply();
						}
						response.setPerformative(ACLMessage.INFORM);
						response.setContent("will ask for time!");

						this.myAgent.addBehaviour(new FindTimeToSettDown(this.getAgent()));

						this.myAgent.send(response);

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;

				}

				else if (request.getContent().startsWith("TimeOff")) {
					try {

						if (response == null) {
							response = request.createReply();
						}
						response.setPerformative(ACLMessage.INFORM);

						final long CURRENT_TIME_MILLIS = System.currentTimeMillis();

						Date dateCurrent = new Date(CURRENT_TIME_MILLIS);
						SimpleDateFormat dateInMilis = new SimpleDateFormat("HH.mm.ss");
						String time = dateInMilis.format(dateCurrent);
						Date now = new Date();
					    Calendar calendar = Calendar.getInstance();
				        calendar.setTime(now);
				        System.out.println(calendar.get(Calendar.DAY_OF_WEEK)); // the day of the week in numerical format
				        String day = calendar.get(Calendar.DAY_OF_WEEK) + "";
				    

						String thour = time.substring(0, 2);
						String tmin = time.substring(3, 5);
						String tsec = time.substring(6, 8);
						String tday = day.substring(0, 1);
						
					
				
						long tallInMili = (Long.valueOf(tday) * 86400000) + (Long.valueOf(thour) * 3600000) + (Long.valueOf(tmin) * 60000)
								+ (Long.valueOf(tsec) * 1000);
						
						System.out.println(""+ tallInMili);

						String timeToOff = request.getContent().substring(7);
						String hour = timeToOff.substring(0, 2);
						String min = timeToOff.substring(3, 5);
						String sec = timeToOff.substring(6, 8);
						String weekday = timeToOff.substring(9, 10);
						long allInMili = (Long.valueOf(hour) * 3600000) + (Long.valueOf(min) * 60000 )
								+ (Long.valueOf(sec) * 1000 + Long.valueOf(weekday) * 86400000);
						long Results = 0;
					
						if (allInMili > tallInMili)	{ Results = (allInMili - tallInMili);}
						else {Results = allInMili - tallInMili +  86400000*7 ;}
						
System.out.println(Results);
						addBehaviour(new WakerBehaviour(this.getAgent(), Results) {

							/**
							 * 
							 */
							private static final long serialVersionUID = 1L;

							protected void onWake() {

								// TODO Auto-generated method stub

								// das erste mal wird direkt gesagt um die uhrzeit macht das Light aus
								// dann wird ein TickerBehaviour ausgeführt was es nach eine Woche wieder tut!
								this.myAgent.addBehaviour(new LightOffFromTimer(this.getAgent()));
								this.myAgent.addBehaviour(new TimerOffBehaviour(this.getAgent(), 86400000*7));

							}

						});

						response.setContent("timer-function is on");

						this.myAgent.send(response);

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;
				}

				// timerOn

				else if (request.getContent().startsWith("TimeOn")) {
					try {

						if (response == null) {
							response = request.createReply();
						}
						response.setPerformative(ACLMessage.INFORM);

						final long CURRENT_TIME_MILLISS = System.currentTimeMillis();

						Date dateCurrent = new Date(CURRENT_TIME_MILLISS);
						SimpleDateFormat dateInMilis = new SimpleDateFormat("HH.mm.ss");
						String time = dateInMilis.format(dateCurrent);
						 
						Date now = new Date();
						Calendar calendar = Calendar.getInstance();
					        calendar.setTime(now);
					        System.out.println(calendar.get(Calendar.DAY_OF_WEEK)); // the day of the week in numerical format
					        String day = calendar.get(Calendar.DAY_OF_WEEK) + "";
					    

						
					        String tday = day.substring(0, 1);
						String thour = time.substring(0, 2);
						String tmin = time.substring(3, 5);
						String tsec = time.substring(6, 8);
						
						
						
						long tallInMili = (Long.valueOf(tday) * 86400000)+(Long.valueOf(thour) * 3600000) + (Long.valueOf(tmin) * 60000)
								+ (Long.valueOf(tsec) * 1000);

						String timeToOff = request.getContent().substring(6);

						String hour = timeToOff.substring(0, 2);
						String min = timeToOff.substring(3, 5);
						String sec = timeToOff.substring(6, 8);
						String weekday = timeToOff.substring(9, 10);
						
						long allInMili = (Long.valueOf(weekday) * 86400000)+(Long.valueOf(hour) * 3600000) + (Long.valueOf(min) * 60000)
								+ (Long.valueOf(sec) * 1000);

						long Results = 0;
						
						if (allInMili > tallInMili)	{ Results = (allInMili - tallInMili);}
						else {Results = allInMili - tallInMili +  86400000*7 ;}
						System.out.println(Results + "");

						addBehaviour(new WakerBehaviour(this.getAgent(), Results) {
							/**
							 * 
							 */
							private static final long serialVersionUID = 1L;

							protected void onWake() {

								// TODO Auto-generated method stub

								// das erste mal wird direkt gesagt um die uhrzeit macht das Light aus
								// dann wird ein TickerBehaviour ausgeführt was es nach 24 stunden wieder tut!
								this.myAgent.addBehaviour(new LightOnFromTimer(this.getAgent()));
								this.myAgent.addBehaviour(new TimerOnBehaviour(this.getAgent(), 86400000*7));

							}
						});

						response.setContent("timer-function is on");

						this.myAgent.send(response);

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;
				}

				else if (request.getContent().startsWith("RolloDown")) {
					try {

						if (response == null) {
							response = request.createReply();
						}
						response.setPerformative(ACLMessage.INFORM);

						final long CURRENT_TIME_MILLISS = System.currentTimeMillis();

						Date dateCurrent = new Date(CURRENT_TIME_MILLISS);
						SimpleDateFormat dateInMilis = new SimpleDateFormat("HH.mm.ss");
						String time = dateInMilis.format(dateCurrent);
						Date now = new Date();
						Calendar calendar = Calendar.getInstance();
					        calendar.setTime(now);
					        System.out.println(calendar.get(Calendar.DAY_OF_WEEK)); // the day of the week in numerical format
					        String day = calendar.get(Calendar.DAY_OF_WEEK) + "";

					        String tday = day.substring(0, 1);
						String thour = time.substring(0, 2);
						String tmin = time.substring(3, 5);
						String tsec = time.substring(6, 8);
						
						long tallInMili = (Long.valueOf(tday) * 86400000)+(Long.valueOf(thour) * 3600000) + (Long.valueOf(tmin) * 60000)
								+ (Long.valueOf(tsec) * 1000);

						String timeToOff = request.getContent().substring(9);

						String hour = timeToOff.substring(0, 2);
						String min = timeToOff.substring(3, 5);
						String sec = timeToOff.substring(6, 8);
						String weekday = timeToOff.substring(9, 10);
						long allInMili = (Long.valueOf(weekday) * 86400000) +(Long.valueOf(hour) * 3600000) + (Long.valueOf(min) * 60000)
								+ (Long.valueOf(sec) * 1000);

						long Results = 0;
						
						if (allInMili > tallInMili)	{ Results = (allInMili - tallInMili);}
						else {Results = allInMili - tallInMili +  86400000*7 ;}
						System.out.println(Results+ "");

						addBehaviour(new WakerBehaviour(this.getAgent(), Results) {
							/**
							 * 
							 */
							private static final long serialVersionUID = 1L;

							protected void onWake() {

								// TODO Auto-generated method stub

								// das erste mal wird direkt gesagt um die uhrzeit macht das Light aus
								// dann wird ein TickerBehaviour ausgeführt was es nach 24 stunden wieder tut!
								this.myAgent.addBehaviour(new RolloDownFromTimer(this.getAgent()));
								this.myAgent.addBehaviour(new RolloDownBehaviour(this.getAgent(), 86400000*7));

							}
						});

						response.setContent("timer-function is on");

						this.myAgent.send(response);

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;
				}

				else if (request.getContent().startsWith("RolloUp")) {
					try {

						if (response == null) {
							response = request.createReply();
						}
						response.setPerformative(ACLMessage.INFORM);

						final long CURRENT_TIME_MILLISS = System.currentTimeMillis();

						Date dateCurrent = new Date(CURRENT_TIME_MILLISS);
						SimpleDateFormat dateInMilis = new SimpleDateFormat("HH.mm.ss");
						String time = dateInMilis.format(dateCurrent);
						Date now = new Date();
						Calendar calendar = Calendar.getInstance();
					        calendar.setTime(now);
					        System.out.println(calendar.get(Calendar.DAY_OF_WEEK)); // the day of the week in numerical format
					        String day = calendar.get(Calendar.DAY_OF_WEEK) + "";

					        String tday = day.substring(0, 1);
						String thour = time.substring(0, 2);
						String tmin = time.substring(3, 5);
						String tsec = time.substring(6, 8);
						
						long tallInMili = (Long.valueOf(tday) * 86400000)+(Long.valueOf(thour) * 3600000) + (Long.valueOf(tmin) * 60000)
								+ (Long.valueOf(tsec) * 1000);

						String timeToOff = request.getContent().substring(7);

						String hour = timeToOff.substring(0, 2);
						String min = timeToOff.substring(3, 5);
						String sec = timeToOff.substring(6, 8);
						String weekday = timeToOff.substring(9, 10);
						long allInMili = (Long.valueOf(weekday) * 86400000) +(Long.valueOf(hour) * 3600000) + (Long.valueOf(min) * 60000)
								+ (Long.valueOf(sec) * 1000);

						long Results = 0;
						
						if (allInMili > tallInMili)	{ Results = (allInMili - tallInMili);}
						else {Results = allInMili - tallInMili +  86400000*7 ;}

						
						addBehaviour(new WakerBehaviour(this.getAgent(), Results) {
							/**
							 * 
							 */
							private static final long serialVersionUID = 1L;

							protected void onWake() {

								// TODO Auto-generated method stub
								
								// das erste mal wird direkt gesagt um die uhrzeit macht das Light aus
								// dann wird ein TickerBehaviour ausgeführt was es nach 24 stunden wieder tut!
								this.myAgent.addBehaviour(new RolloUpFromTimer(this.getAgent()));
								this.myAgent.addBehaviour(new RolloUpBehaviour(this.getAgent(), 86400000*7));

							}
						});

						response.setContent("timer-function is on");

						this.myAgent.send(response);

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;
				}

				else if (request.getContent().equals("OFF")) {
					try {
						String status = new String(request.getContent());
						if (response == null) {
							response = request.createReply();
						}
						final long CURRENT_TIME_MILLIS = System.currentTimeMillis();
						final Timer timer = new Timer();
						Date dateCurrent = new Date(CURRENT_TIME_MILLIS);
						SimpleDateFormat dateInMilis = new SimpleDateFormat("HH.mm.ss");
						String time = dateInMilis.format(dateCurrent);

						response.setPerformative(ACLMessage.INFORM);
						light2.setStatus(off);
						light1.setStatus(off);

						this.myAgent.addBehaviour(new StartSever(this.myAgent));

						response.setContent("all Lights are Off now" + time);

						this.myAgent.send(response);

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;

				}

				else if (request.getContent().equals("UP")) {
					try {
						String status = new String(request.getContent());
						if (response == null) {
							response = request.createReply();
						}
						final long CURRENT_TIME_MILLIS = System.currentTimeMillis();
						final Timer timer = new Timer();
						Date dateCurrent = new Date(CURRENT_TIME_MILLIS);
						SimpleDateFormat dateInMilis = new SimpleDateFormat("HH.mm.ss");
						String time = dateInMilis.format(dateCurrent);

						response.setPerformative(ACLMessage.INFORM);
						rollo1.setStatus(up);
						rollo2.setStatus(up);

						// this.myAgent.addBehaviour(new StartSever(this.myAgent));

						response.setContent("all Rollos are Up now" + time);

						this.myAgent.send(response);

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;

				}
				else if (request.getContent().equals("stop")) {
					try {
						String status = new String(request.getContent());
						if (response == null) {
							response = request.createReply();
						}
						
						// hier sollte eine methode sein, was diese Agent clean und bereits macht
						takeDown();
						

					

						response.setContent("stop now");

						this.myAgent.send(response);

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;

				}

				else if (request.getContent().equals("DOWN")) {
					try {
						String status = new String(request.getContent());
						if (response == null) {
							response = request.createReply();
						}
						final long CURRENT_TIME_MILLIS = System.currentTimeMillis();
						final Timer timer = new Timer();
						Date dateCurrent = new Date(CURRENT_TIME_MILLIS);
						SimpleDateFormat dateInMilis = new SimpleDateFormat("HH.mm.ss");
						String time = dateInMilis.format(dateCurrent);

						response.setPerformative(ACLMessage.INFORM);
						rollo1.setStatus(down);
						rollo2.setStatus(down);

						// this.myAgent.addBehaviour(new StartSever(this.myAgent));

						response.setContent("all Rollos are Down now" + time);

						this.myAgent.send(response);

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;

				}

				else if (request.getContent().equals("ON")) {
					try {
						String status = new String(request.getContent());
						if (response == null) {
							response = request.createReply();
						}
						final long CURRENT_TIME_MILLIS = System.currentTimeMillis();
						final Timer timer = new Timer();
						Date dateCurrent = new Date(CURRENT_TIME_MILLIS);
						SimpleDateFormat dateInMilis = new SimpleDateFormat("HH.mm.ss");
						String time = dateInMilis.format(dateCurrent);

						response.setPerformative(ACLMessage.INFORM);
						light2.setStatus(on);
						light1.setStatus(on);
						response.setContent("all Lights are On now" + time);

						this.myAgent.send(response);

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;

				}

				return super.prepareResultNotification(request, response);
			}

		});
	}


	@Override
	public void takeDown() {
		try {
			DFService.deregister(this);

		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}
}
