package agent;

import Protocols.ServeStatusBehaviourJalousie1;
import Protocols.ServeStatusBehaviourJalousie2;
import Protocols.ServeStatusBehaviourLight1;
import Protocols.ServeStatusBehaviourLight2;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;
import smartHOntology.RoomOntology;

public class RoomAgent extends Agent {
	/**
	 * diese Agent verwaltet die Status-Abfragen function
	 * Schnittstelle zwischen UserAgent und DeviceAgent
	 * @author  Phan Anh Tran
	 * 
	 */
	private Codec codec = new SLCodec();

	/**
	 * ontology used for semantic parsing
	 */
	private Ontology ontology = RoomOntology.getInstance();

	/**
	 * for serializing
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * only for testing, would be DB Access in RL
	 */

	/**
	 * agent initializing
	 */

	// Reference to the gui

	// Instanciate the gui

	// super.setup();
	@Override
	protected void setup() {

		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(this.getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("Conect-Service");
		sd.setName(this.getLocalName() + "-Room-Conect-Service");
		// add Ontologies, Languages, Interaction Protocols here
		dfd.addServices(sd);
		try {
			DFService.register(this, dfd);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}

		// Register SLCodec & RoomOntology with MessageParser (CM)
		this.getContentManager().registerLanguage(this.codec);
		this.getContentManager().registerOntology(ontology);

		MessageTemplate mt = AchieveREResponder.createMessageTemplate(FIPANames.InteractionProtocol.FIPA_REQUEST);
		this.addBehaviour(new AchieveREResponder(this, mt) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
				// TODO Auto-generated method stub
				ACLMessage response = request.createReply();
				try {
				} catch (Exception e) {
					response.setPerformative(ACLMessage.NOT_UNDERSTOOD);
					response.setContent("Wrong Room or Device");
					throw new NotUnderstoodException(response);
				}
				response.setPerformative(ACLMessage.AGREE);
				return response;
			}

			// erste anfrage nach der Conection
			@Override
			protected ACLMessage prepareResultNotification(ACLMessage request, ACLMessage response)
					throws FailureException {

				if (request.getContent().equals("light1")) {

					try {
						if (response == null) {
							response = request.createReply();
						}
						response.setPerformative(ACLMessage.INFORM);

						this.myAgent.send(response);

						this.myAgent.addBehaviour(new ServeStatusBehaviourLight1(this.getAgent()));

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;

				}
				if (request.getContent().equals("jalousie1")) {

					try {
						if (response == null) {
							response = request.createReply();
						}
						response.setPerformative(ACLMessage.INFORM);

						this.myAgent.send(response);

						this.myAgent.addBehaviour(new ServeStatusBehaviourJalousie1(this.getAgent()));

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;

				}
				if (request.getContent().equals("jalousie2")) {

					try {
						if (response == null) {
							response = request.createReply();
						}
						response.setPerformative(ACLMessage.INFORM);

						this.myAgent.send(response);

						this.myAgent.addBehaviour(new ServeStatusBehaviourJalousie2(this.getAgent()));

					}

					catch (Exception e) {
						throw new FailureException(response);
					}
					return response;

				}

				else if (request.getContent().equals("light2")) {
					try {
						String status = new String(request.getContent());
						if (response == null) {
							response = request.createReply();

						}
						response.setPerformative(ACLMessage.INFORM);
						this.myAgent.send(response);
						this.myAgent.addBehaviour(new ServeStatusBehaviourLight2(this.getAgent()));
					} catch (Exception e) {
						throw new FailureException(response);
					}
					return response;
				}

				// TODO Auto-generated method stub
				return super.prepareResultNotification(request, response);
			}

		});
	}

	@Override
	protected void takeDown() {
		super.takeDown();
	}

}
