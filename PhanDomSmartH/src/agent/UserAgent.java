package agent;

import java.awt.Color;

import Protocols.TelldeviceAgentTimeToSetDownRollo;
import Protocols.TelldeviceAgentTimeToSetOff;
import Protocols.TelldeviceAgentTimeToSetOn;
import Protocols.TelldeviceAgentTimeToSetUpRollo;
import gui.Gui;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.AchieveREResponder;
import jade.util.leap.Serializable;

public class UserAgent extends Agent implements Serializable{

	
	
	/**
	 * dieser Agent kommuniziert mit der Gui 
	 * und leitet User-eingegebene Daten weiter an anderen Agenten
	 * @author  Phan Anh Tran
	 */
	private static final long serialVersionUID = 1L;

	transient public static Gui myGui;

	private AID roomAgent;
	
	protected String status = "Current Status :      ";
	

	
	public AID getRoomAgentAID(){
		return roomAgent;
	}

	
	@Override
	protected void setup() {


		myGui = new Gui(this);
		myGui.setVisible(true);
		
	
		

		DFAgentDescription dfd = new DFAgentDescription();
	    dfd.setName( this.getAID() );
	    ServiceDescription sd = new ServiceDescription();
	    sd.setType( "Answer-Service" );
	    sd.setName(this.getLocalName() + "-Answer-Service" );
	    //add Ontologies, Languages, Interaction Protocols here
	    dfd.addServices( sd );
	    try{
	      DFService.register( this, dfd );
	    }
	    catch( FIPAException fe ){
	      fe.printStackTrace();
	    }
		
	 
		 
//		String time = UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue());
	  
	        
	     
	    MessageTemplate mt = AchieveREResponder.createMessageTemplate(FIPANames.InteractionProtocol.FIPA_REQUEST);
	    this.addBehaviour(new AchieveREResponder(this, mt)
	    {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			
				@Override
				protected ACLMessage handleRequest(ACLMessage request) throws NotUnderstoodException, RefuseException {
					// TODO Auto-generated method stub
					ACLMessage response = request.createReply();
					try {
					} catch (Exception e) {
						response.setPerformative(ACLMessage.NOT_UNDERSTOOD);
						response.setContent("Not-understand");
						throw new NotUnderstoodException(response);
					}
					response.setPerformative(ACLMessage.AGREE);
					return response;
				}
				@Override
				protected ACLMessage prepareResultNotification(ACLMessage request, ACLMessage response)
						throws FailureException {
		 if (request.getContent().equals("light2on")) {
							try {
							
								if (response == null) {
									response = request.createReply();
									
									
								}
								response.setPerformative(ACLMessage.INFORM);
								
								response.setContent("on");
								this.myAgent.send(response);
								myGui.statusArea.setValue(status +"on");
							
									myGui.statusArea.setForeground(Color.GREEN);
							
								;
		
								
								this.myAgent.send(response);
								
							}
								catch (Exception e) {
									throw new FailureException(response);
								}
								return response;
								}
						 
						 if (request.getContent().equals("light2off")) {
								try {
									
									if (response == null) {
										response = request.createReply();
										
										
									}
									response.setPerformative(ACLMessage.INFORM);
									
									response.setContent("off");
									this.myAgent.send(response);
									myGui.statusArea.setValue(status + "off");
								
									
								
										myGui.statusArea.setForeground(Color.YELLOW);
			
									
							
									
								}
									catch (Exception e) {
										throw new FailureException(response);
									}
									return response;
									}
						 
						 
						 else if (request.getContent().equals("jalousie1on")) {
								try {
								
									if (response == null) {
										response = request.createReply();
										
										
									}
									response.setPerformative(ACLMessage.INFORM);
									
									response.setContent("down");
									this.myAgent.send(response);
									myGui.statusArea.setValue(status + "down");
								
										myGui.statusArea.setForeground(Color.GREEN);
								
										
			
									
									
								}
									catch (Exception e) {
										throw new FailureException(response);
									}
									return response;
									}

						 
						 else if (request.getContent().equals("jalousie2on")) {
								try {
								
									if (response == null) {
										response = request.createReply();
										
										
									}
									response.setPerformative(ACLMessage.INFORM);
									
									response.setContent("down");
									this.myAgent.send(response);
									myGui.statusArea.setValue(status + "down");
								
										myGui.statusArea.setForeground(Color.GREEN);
								
										
			
									
									
								}
									catch (Exception e) {
										throw new FailureException(response);
									}
									return response;
									}

						 
						 else if (request.getContent().equals("jalousie1off")) {
								try {
							
									if (response == null) {
										response = request.createReply();
										
										
									}
									response.setPerformative(ACLMessage.INFORM);
									
									response.setContent("up");
									this.myAgent.send(response);
									myGui.statusArea.setValue(status + "up");
								
										myGui.statusArea.setForeground(Color.YELLOW);
								
										
			
									
									
								}
									catch (Exception e) {
										throw new FailureException(response);
									}
									return response;
									}
						 
						 else if (request.getContent().equals("jalousie2off")) {
								try {
									
									if (response == null) {
										response = request.createReply();
										
										
									}
									response.setPerformative(ACLMessage.INFORM);
									
									response.setContent("up");
									this.myAgent.send(response);
									myGui.statusArea.setValue(status +  "up");
								
										myGui.statusArea.setForeground(Color.YELLOW);
								
										
			
									
									
								}
									catch (Exception e) {
										throw new FailureException(response);
									}
									return response;
									}

						 

						 else if (request.getContent().equals("light1on")) {
								try {
									
									if (response == null) {
										response = request.createReply();
										
										
									}
									response.setPerformative(ACLMessage.INFORM);
									
									response.setContent("on");
									this.myAgent.send(response);
									myGui.statusArea.setValue(status + "on");
								
										myGui.statusArea.setForeground(Color.GREEN);
								
										
			
									
									
								}
									catch (Exception e) {
										throw new FailureException(response);
									}
									return response;
									}

						 if (request.getContent().equals("light1off")) {
								try {
									if (response == null) {
										response = request.createReply();
										
										
									}
									response.setPerformative(ACLMessage.INFORM);
									
									response.setContent("off");
									this.myAgent.send(response);
									myGui.statusArea.setValue(status + "off");
								
									
								
										myGui.statusArea.setForeground(Color.YELLOW);
			
									
									
								}
									catch (Exception e) {
										throw new FailureException(response);
									}
									return response;
									}
						 

						 if (request.getContent().equals("timeOff?")) {
								try {
									if (response == null) {
										response = request.createReply();
										
										
									}
									response.setPerformative(ACLMessage.INFORM);
									
									response.setContent("sending time!");
									this.myAgent.addBehaviour( new TelldeviceAgentTimeToSetOff(this.getAgent()) );
							
									
									
								}
									catch (Exception e) {
										throw new FailureException(response);
									}
									return response;
									}
						 if (request.getContent().equals("timeUp?")) {
								try {
									if (response == null) {
										response = request.createReply();
										
										
									}
									response.setPerformative(ACLMessage.INFORM);
									
									response.setContent("sending time!");
									this.myAgent.addBehaviour( new TelldeviceAgentTimeToSetUpRollo(this.getAgent()) );
							
									
									
								}
									catch (Exception e) {
										throw new FailureException(response);
									}
									return response;
									}
						 if (request.getContent().equals("timeDown?")) {
								try {
									if (response == null) {
										response = request.createReply();
										
										
									}
									response.setPerformative(ACLMessage.INFORM);
									
									response.setContent("sending time!");
									this.myAgent.addBehaviour( new TelldeviceAgentTimeToSetDownRollo(this.getAgent()) );
							
									
									
								}
									catch (Exception e) {
										throw new FailureException(response);
									}
									return response;
									}
						

						 if (request.getContent().equals("timeOn?")) {
								try {
									
									if (response == null) {
										response = request.createReply();
										
										
									}
									response.setPerformative(ACLMessage.INFORM);
									
									response.setContent("sending time!");
									this.myAgent.addBehaviour( new TelldeviceAgentTimeToSetOn(this.getAgent()) );
							
									
									
								}
									catch (Exception e) {
										throw new FailureException(response);
									}
									return response;
									}
						 
					// TODO Auto-generated method stub
					return super.prepareResultNotification(request, response);
						}

					});
				}

	

		@Override
		protected void takeDown() {
			try {
				DFService.deregister(this);

			} catch (FIPAException fe) {
				fe.printStackTrace();
			}
		}

}
