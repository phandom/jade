package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.Box;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerDateModel;
import javax.swing.border.BevelBorder;
import javax.swing.event.DocumentListener;

import Protocols.AllLightOffBehaviour;
import Protocols.AllLightOnBehaviour;
import Protocols.AllRollosDownBehaviour;
import Protocols.AllRollosUpBehaviour;
import Protocols.RoomFindBehaviour;
import agent.RoomAgent;
import agent.UserAgent;

public class Gui extends JFrame {

	/**
	 * @author  Phan Anh Tran
	 */
	private static final long serialVersionUID = 1L;

	public JLabel device = new JLabel("");

	public JComboBox<?> deviceName = new JComboBox<Object>(
			new Object[] { "light1", "light2", "jalousie1", "jalousie2" });
	private JComboBox<?> internal;

	private JComboBox<?> getInternalCombobox() {
		if (internal == null) {
			internal = new JComboBox<Object>(new Object[] { null });
		}
		return internal;
	}

	public JComboBox<?> daysBox = new JComboBox<Object>(
			new Object[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" });
	private JComboBox<?> internal2;

	private JComboBox<?> getInternalCombobox2() {
		if (internal2 == null) {
			internal2 = new JComboBox<Object>(new Object[] { null });
		}

		return internal2;
	}

	private JLabel statusShow = new JLabel("");
	public JFormattedTextField statusArea = new JFormattedTextField();

	protected JFrame frame = new JFrame("Timer");
	protected JFrame timeFrame = new JFrame("Setting");
	
	public RoomAgent roomAgent;
	public UserAgent myAgent;
	public JTextArea timerArea = new JTextArea();

	private JPanel panel = new JPanel();

	public JButton statuschange = null;
	final JButton RESET = new JButton("Reset");
	private JButton lightOff = null;
	private JButton lightOn = null;
	private JButton jalousieOff = null;
	private JButton jalousieOn = null;

	private JButton timer = null;

	Date date = new Date();
	public SpinnerDateModel sm = new SpinnerDateModel(date, null, null, Calendar.MINUTE);
	public JSpinner timeSpinner = new JSpinner(sm);

	public SpinnerDateModel sm2 = new SpinnerDateModel(date, null, null, Calendar.MINUTE);
	public JSpinner timeSpinner2 = new JSpinner(sm2);
	public JSpinner.DateEditor de = new JSpinner.DateEditor(timeSpinner, "HH.mm.ss ");
	public JSpinner.DateEditor deOff = new JSpinner.DateEditor(timeSpinner2, "HH.mm.ss ");

	// convert back to milisecond ?

	DocumentListener listener;

	JPanel buttonpane = new JPanel();

	// timespinner bei Setting-Window

	public Gui(UserAgent agent) {
		super();
		this.myAgent = agent;

		this.setPreferredSize(new Dimension(500, 300));

		this.setSize(new Dimension(500, 300));

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		timerArea.setWrapStyleWord(true);
		timerArea.setLineWrap(true);

		statuschange = new JButton("status");
		statuschange.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {
				if (event.getActionCommand().equalsIgnoreCase(statuschange.getText())) {

					myAgent.addBehaviour(new RoomFindBehaviour(agent));

				}

			}
		});


		// public JComboBox daysBox = new JComboBox(new Object[] {"Sunday", "Monday",
		// "Tuesday",
		// "Wednesday", "Thursday", "Friday", "Saturday"} );

		Box box1 = Box.createVerticalBox();
		Box box3 = Box.createVerticalBox();

		JPanel panel2 = new JPanel();

		panel2.add(new JLabel("all Devices setting"));

		box1.add(panel2);
		JPanel panel5 = new JPanel();
		panel5.add(daysBox);
		panel5.add(RESET);

		box1.add(panel5);

		JPanel panel1 = new JPanel();

		panel1.add(new JLabel("Turn Off/Up at specific time"));
		panel1.add(timeSpinner);
		timeSpinner.setEditor(de);
		timeSpinner.setValue(new Date());

		panel1.add(lightOff = new JButton("Light"));
		panel1.add(jalousieOff = new JButton("Rollo"));

		box1.add(panel1);

		JPanel panel3 = new JPanel();
		panel3.add(new JLabel("Turn On/Down at specific time"));
		panel3.add(timeSpinner2);
		timeSpinner2.setEditor(deOff);
		timeSpinner2.setValue(new Date());
		panel3.add(lightOn = new JButton("Light"));
		panel3.add(jalousieOn = new JButton("Rollo"));
		box1.add(panel3);

		box1.add(timerArea);
		timerArea.setEditable(false);
	

		lightOff.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {

				myAgent.addBehaviour(new AllLightOffBehaviour(agent));

			}
		});


		lightOn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {

				myAgent.addBehaviour(new AllLightOnBehaviour(agent));

			}
		});

		jalousieOff.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {

				myAgent.addBehaviour(new AllRollosUpBehaviour(agent));

			}
		});

		jalousieOn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {

				myAgent.addBehaviour(new AllRollosDownBehaviour(agent));

			}
		});

		// second Window for time-setting

		timeFrame.setVisible(false);
		timeFrame.getContentPane().add(box1, BorderLayout.CENTER);
		Box box = Box.createVerticalBox();

		box.setBorder(new BevelBorder(0));

		box.add(Box.createVerticalStrut(10));

		box.add(deviceName);

		timeFrame.setSize(500, 300);

		// set-up button führt zur setting-window
		timer = new JButton("timer" + "");
		timer.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent event) {
				if (event.getActionCommand().equalsIgnoreCase(timer.getText())) {
					timeFrame.setVisible(true);

				}

			}
		});

		box.add(Box.createVerticalStrut(10));
		box.add(statusShow);

		box.add(Box.createVerticalStrut(5));
		box.add(statusArea);
		statusArea.setEditable(false);
		statusArea.setBackground(Color.BLACK);

		JPanel panel4 = new JPanel();
		panel4.add(Box.createVerticalStrut(10));

		panel4.add(statuschange);
		panel4.add(Box.createVerticalStrut(10));
		panel4.add(timer);
		box.add(panel4);

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(box, BorderLayout.CENTER);
		// this.getContentPane().add( this.panel );

		this.panel.setLayout(new FlowLayout());
		
		
		
		
		RESET.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {

				
				repaint();
				
				daysBox.setSelectedItem(null);
				
				AllLightOffBehaviour.lighOffMessageSunday = null;
				AllLightOffBehaviour.lighOffMessageMon = null;
				AllLightOffBehaviour.lighOffMessageSat = null;
				AllLightOffBehaviour.lighOffMessageFri = null;
				AllLightOffBehaviour.lighOffMessageThir = null;
				AllLightOffBehaviour.lighOffMessageTue = null;
				AllLightOffBehaviour.lighOffMessageWende = null;

				// light On for every single day

				AllLightOnBehaviour.lighOnMessageMon = null;
				AllLightOnBehaviour.lighOnMessageSat = null;
				AllLightOnBehaviour.lighOnMessageFri = null;
				AllLightOnBehaviour.lighOnMessageThir = null;
				AllLightOnBehaviour.lighOnMessageTue = null;
				AllLightOnBehaviour.lighOnMessageWende = null;

				// JAslouse up and down

				AllRollosDownBehaviour.RollosOnMessageMon = null;
				AllRollosDownBehaviour.RollosOnMessageSat = null;
				AllRollosDownBehaviour.RollosOnMessageFri = null;
				AllRollosDownBehaviour.RollosOnMessageThir = null;
				AllRollosDownBehaviour.RollosOnMessageTue = null;
				AllRollosDownBehaviour.RollosOnMessageWende = null;
				AllRollosDownBehaviour.RollosOnMessageSunday = null;

				AllRollosUpBehaviour.RollosOffMessageMon = null;
				AllRollosUpBehaviour.RollosOffMessageSat = null;
				AllRollosUpBehaviour.RollosOffMessageFri = null;
				AllRollosUpBehaviour.RollosOffMessageThir = null;
				AllRollosUpBehaviour.RollosOffMessageTue = null;
				AllRollosUpBehaviour.RollosOffMessageWende = null;
				AllRollosUpBehaviour.RollosOffMessageSunday=null;
			
				
			}
		});

		
		//statusScreenInfo setting

		deviceName.setRenderer(new DefaultListCellRenderer() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
					boolean cellHasFocus) {
				Component comp = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				if (value == null) {
					setText("Please Choose a device!");
					statusArea.setForeground(Color.BLACK);
					statusArea.setBackground(Color.BLACK);
					
					statuschange.setEnabled(false);
				} else {
					statuschange.setEnabled(true);
				
					statusArea.setBackground(Color.GRAY);
				}
				return comp;

			}
		});

		deviceName.setSelectedItem(null);
		
		//TimerInfoScreen setting

		daysBox.setRenderer(new DefaultListCellRenderer() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
					boolean cellHasFocus) {
				Component comp2 = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				if (value == null) {
					setText("Select Day");
					timerArea.setForeground(Color.BLACK);
					timerArea.setBackground(Color.BLACK);

					timeSpinner.setEnabled(false);
					timeSpinner2.setEnabled(false);
					lightOff.setEnabled(false);
					lightOn.setEnabled(false);
					jalousieOff.setEnabled(false);
					jalousieOn.setEnabled(false);

				} else if (value != null) {
					timerArea.setBackground(Color.GRAY);
					String defaultText = "no Timer-setting";
					if (AllLightOffBehaviour.lighOffMessageSunday == null) {
						AllLightOffBehaviour.lighOffMessageSunday = (defaultText);
					} else if (AllLightOffBehaviour.lighOffMessageMon == null) {
						AllLightOffBehaviour.lighOffMessageMon = (defaultText);
					} else if (AllLightOffBehaviour.lighOffMessageTue == null) {
						AllLightOffBehaviour.lighOffMessageTue = (defaultText);
					} else if (AllLightOffBehaviour.lighOffMessageSat == null) {
						AllLightOffBehaviour.lighOffMessageSat = (defaultText);
					} else if (AllLightOffBehaviour.lighOffMessageFri == null) {
						AllLightOffBehaviour.lighOffMessageFri = (defaultText);
					} else if (AllLightOffBehaviour.lighOffMessageThir == null) {
						AllLightOffBehaviour.lighOffMessageThir = (defaultText);
					} else if (AllLightOffBehaviour.lighOffMessageWende == null) {
						AllLightOffBehaviour.lighOffMessageWende = (defaultText);
					} else if (AllLightOnBehaviour.lighOnMessageMon == null) {
						AllLightOnBehaviour.lighOnMessageMon = (defaultText);
					} else if (AllLightOnBehaviour.lighOnMessageFri == null) {
						AllLightOnBehaviour.lighOnMessageFri = (defaultText);
					} else if (AllLightOnBehaviour.lighOnMessageSat == null) {
						AllLightOnBehaviour.lighOnMessageSat = (defaultText);
					} else if (AllLightOnBehaviour.lighOnMessageSunday == null) {
						AllLightOnBehaviour.lighOnMessageSunday = (defaultText);
					} else if (AllLightOnBehaviour.lighOnMessageThir == null) {
						AllLightOnBehaviour.lighOnMessageThir = (defaultText);
					} else if (AllLightOnBehaviour.lighOnMessageTue == null) {
						AllLightOnBehaviour.lighOnMessageTue = (defaultText);
					} else if (AllLightOnBehaviour.lighOnMessageWende == null) {
						AllLightOnBehaviour.lighOnMessageWende = (defaultText);
					} else if (AllRollosDownBehaviour.RollosOnMessageFri == null) {
						AllRollosDownBehaviour.RollosOnMessageFri = (defaultText);
					} else if (AllRollosDownBehaviour.RollosOnMessageMon == null) {
						AllRollosDownBehaviour.RollosOnMessageMon = (defaultText);
					} else if (AllRollosDownBehaviour.RollosOnMessageSat == null) {
						AllRollosDownBehaviour.RollosOnMessageSat = (defaultText);
					} else if (AllRollosDownBehaviour.RollosOnMessageSunday == null) {
						AllRollosDownBehaviour.RollosOnMessageSunday = (defaultText);
					} else if (AllRollosDownBehaviour.RollosOnMessageThir == null) {
						AllRollosDownBehaviour.RollosOnMessageThir = (defaultText);
					} else if (AllRollosDownBehaviour.RollosOnMessageTue == null) {
						AllRollosDownBehaviour.RollosOnMessageTue = (defaultText);
					} else if (AllRollosDownBehaviour.RollosOnMessageWende == null) {
						AllRollosDownBehaviour.RollosOnMessageWende = (defaultText);
					} else if (AllRollosUpBehaviour.RollosOffMessageFri == null) {
						AllRollosUpBehaviour.RollosOffMessageFri = (defaultText);
					} else if (AllRollosUpBehaviour.RollosOffMessageMon == null) {
						AllRollosUpBehaviour.RollosOffMessageMon = (defaultText);
					} else if (AllRollosUpBehaviour.RollosOffMessageSat == null) {
						AllRollosUpBehaviour.RollosOffMessageSat = (defaultText);
					} else if (AllRollosUpBehaviour.RollosOffMessageSunday == null) {
						AllRollosUpBehaviour.RollosOffMessageSunday = (defaultText);
					} else if (AllRollosUpBehaviour.RollosOffMessageThir == null) {
						AllRollosUpBehaviour.RollosOffMessageThir = (defaultText);
					} else if (AllRollosUpBehaviour.RollosOffMessageTue == null) {
						AllRollosUpBehaviour.RollosOffMessageTue = (defaultText);
					} else if (AllRollosUpBehaviour.RollosOffMessageWende == null) {
						AllRollosUpBehaviour.RollosOffMessageWende = (defaultText);
					}

					else {

						// lightOff for every single day
						AllLightOffBehaviour.lighOffMessageSunday = AllLightOffBehaviour.lighOffMessageSunday;
						AllLightOffBehaviour.lighOffMessageMon = AllLightOffBehaviour.lighOffMessageMon;
						AllLightOffBehaviour.lighOffMessageSat = AllLightOffBehaviour.lighOffMessageSat;
						AllLightOffBehaviour.lighOffMessageFri = AllLightOffBehaviour.lighOffMessageFri;
						AllLightOffBehaviour.lighOffMessageThir = AllLightOffBehaviour.lighOffMessageThir;
						AllLightOffBehaviour.lighOffMessageTue = AllLightOffBehaviour.lighOffMessageTue;
						AllLightOffBehaviour.lighOffMessageWende = AllLightOffBehaviour.lighOffMessageWende;

						// light On for every single day

						AllLightOnBehaviour.lighOnMessageMon = AllLightOnBehaviour.lighOnMessageMon;
						AllLightOnBehaviour.lighOnMessageSat = AllLightOnBehaviour.lighOnMessageSat;
						AllLightOnBehaviour.lighOnMessageFri = AllLightOnBehaviour.lighOnMessageFri;
						AllLightOnBehaviour.lighOnMessageThir = AllLightOnBehaviour.lighOnMessageThir;
						AllLightOnBehaviour.lighOnMessageTue = AllLightOnBehaviour.lighOnMessageTue;
						AllLightOnBehaviour.lighOnMessageWende = AllLightOnBehaviour.lighOnMessageWende;

						// JAslouse up and down

						AllRollosDownBehaviour.RollosOnMessageMon = AllRollosDownBehaviour.RollosOnMessageMon;
						AllRollosDownBehaviour.RollosOnMessageSat = AllRollosDownBehaviour.RollosOnMessageSat;
						AllRollosDownBehaviour.RollosOnMessageFri = AllRollosDownBehaviour.RollosOnMessageFri;
						AllRollosDownBehaviour.RollosOnMessageThir = AllRollosDownBehaviour.RollosOnMessageThir;
						AllRollosDownBehaviour.RollosOnMessageTue = AllRollosDownBehaviour.RollosOnMessageTue;
						AllRollosDownBehaviour.RollosOnMessageWende = AllRollosDownBehaviour.RollosOnMessageWende;

						AllRollosUpBehaviour.RollosOffMessageMon = AllRollosUpBehaviour.RollosOffMessageMon;
						AllRollosUpBehaviour.RollosOffMessageSat = AllRollosUpBehaviour.RollosOffMessageSat;
						AllRollosUpBehaviour.RollosOffMessageFri = AllRollosUpBehaviour.RollosOffMessageFri;
						AllRollosUpBehaviour.RollosOffMessageThir = AllRollosUpBehaviour.RollosOffMessageThir;
						AllRollosUpBehaviour.RollosOffMessageTue = AllRollosUpBehaviour.RollosOffMessageTue;
						AllRollosUpBehaviour.RollosOffMessageWende = AllRollosUpBehaviour.RollosOffMessageWende;
					}

					if (value.equals("Sunday")) {
						timerArea.setForeground(Color.BLACK);

						timerArea.setText("*****************************" + "\n"
								+ AllLightOffBehaviour.lighOffMessageSunday + "\n"
								+ AllLightOnBehaviour.lighOnMessageSunday + "\n" + "*****************************"
								+ "\n" + AllRollosDownBehaviour.RollosOnMessageSunday + "\n"
								+ AllRollosUpBehaviour.RollosOffMessageSunday + "\n" + "*****************************");
						lightOff.setEnabled(true);
						lightOn.setEnabled(true);
						jalousieOff.setEnabled(true);
						jalousieOn.setEnabled(true);
						timeSpinner.setEnabled(true);
						timeSpinner2.setEnabled(true);

					}

					else if (value.equals("Monday")) {
						timerArea.setForeground(Color.BLACK);
						timerArea.setText("*****************************" + "\n"
								+ AllLightOffBehaviour.lighOffMessageMon + "\n" + AllLightOnBehaviour.lighOnMessageMon
								+ "\n" + "*****************************" + "\n"
								+ AllRollosDownBehaviour.RollosOnMessageMon + "\n"
								+ AllRollosUpBehaviour.RollosOffMessageMon + "\n" + "*****************************");
						lightOff.setEnabled(true);
						lightOn.setEnabled(true);
						jalousieOff.setEnabled(true);
						jalousieOn.setEnabled(true);
						timeSpinner.setEnabled(true);
						timeSpinner2.setEnabled(true);

					} else if (value.equals("Tuesday")) {
						timerArea.setForeground(Color.BLACK);
						timerArea.setText("*****************************" + "\n"
								+ AllLightOffBehaviour.lighOffMessageTue + "\n" + AllLightOnBehaviour.lighOnMessageTue
								+ "\n" + "*****************************" + "\n"
								+ AllRollosDownBehaviour.RollosOnMessageTue + "\n"
								+ AllRollosUpBehaviour.RollosOffMessageTue + "\n" + "*****************************");
						lightOff.setEnabled(true);
						lightOn.setEnabled(true);
						jalousieOff.setEnabled(true);
						jalousieOn.setEnabled(true);
						timeSpinner.setEnabled(true);
						timeSpinner2.setEnabled(true);

					} else if (value.equals("Wednesday")) {
						timerArea.setForeground(Color.BLACK);
						timerArea.setText("*****************************" + "\n"
								+ AllLightOffBehaviour.lighOffMessageWende + "\n"
								+ AllLightOnBehaviour.lighOnMessageWende + "\n" + "*****************************" + "\n"
								+ AllRollosDownBehaviour.RollosOnMessageWende + "\n"
								+ AllRollosUpBehaviour.RollosOffMessageWende + "\n" + "*****************************");
						lightOff.setEnabled(true);
						lightOn.setEnabled(true);
						jalousieOff.setEnabled(true);
						jalousieOn.setEnabled(true);
						timeSpinner.setEnabled(true);
						timeSpinner2.setEnabled(true);
					} else if (value.equals("Thursday")) {
						timerArea.setForeground(Color.BLACK);
						timerArea.setText("*****************************" + "\n"
								+ AllLightOffBehaviour.lighOffMessageThir + "\n" + AllLightOnBehaviour.lighOnMessageThir
								+ "\n" + "*****************************" + "\n"
								+ AllRollosDownBehaviour.RollosOnMessageThir + "\n"
								+ AllRollosUpBehaviour.RollosOffMessageThir + "\n" + "*****************************");
						lightOff.setEnabled(true);
						lightOn.setEnabled(true);
						jalousieOff.setEnabled(true);
						jalousieOn.setEnabled(true);
						timeSpinner.setEnabled(true);
						timeSpinner2.setEnabled(true);
					} else if (value.equals("Friday")) {
						timerArea.setForeground(Color.BLACK);
						timerArea.setText("*****************************" + "\n"
								+ AllLightOffBehaviour.lighOffMessageFri + "\n" + AllLightOnBehaviour.lighOnMessageFri
								+ "\n" + "*****************************" + "\n"
								+ AllRollosDownBehaviour.RollosOnMessageFri + "\n"
								+ AllRollosUpBehaviour.RollosOffMessageFri + "\n" + "*****************************");
						lightOff.setEnabled(true);
						lightOn.setEnabled(true);
						jalousieOff.setEnabled(true);
						jalousieOn.setEnabled(true);
						timeSpinner.setEnabled(true);
						timeSpinner2.setEnabled(true);
						timerArea.setForeground(Color.BLACK);
					} else if (value.equals("Saturday")) {

						timerArea.setText("*****************************" + "\n"
								+ AllLightOffBehaviour.lighOffMessageSat + "\n" + AllLightOnBehaviour.lighOnMessageSat
								+ "\n" + "*****************************" + "\n"
								+ AllRollosDownBehaviour.RollosOnMessageSat + "\n"
								+ AllRollosUpBehaviour.RollosOffMessageSat + "\n" + "*****************************");
						lightOff.setEnabled(true);
						lightOn.setEnabled(true);
						jalousieOff.setEnabled(true);
						jalousieOn.setEnabled(true);
						timeSpinner.setEnabled(true);
						timeSpinner2.setEnabled(true);

					}
				}
				return comp2;

			}

		});
		daysBox.setSelectedItem(null);

	}

}