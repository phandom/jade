package smartHOntology;

import jade.content.AgentAction;
import jade.core.AID;

public class LightOn implements AgentAction {
	 private static final long serialVersionUID = 1L;
	  
	  /**
	   * who wants to get the book
	   */
	  private AID requester = null;
	//  private InCatalogue predicate = null;
	  
	  /**
	   * only one book per action
	   */
	  private Light lightToOn = null;
	  
	  
	  public LightOn()
	  {
	    super();
	  }
	  
	  
	  public AID getRequester()
	  {
	    return requester;
	  }
	  public void setRequester(AID requester)
	  {
	    this.requester = requester;
	  }

	  public Light getLightToOn()
	  {
	    return lightToOn;
	  }

	  public void setLightToOn(Light lightToOn)
	  {
	    this.lightToOn = lightToOn;
	  }

	  
	  
	}


