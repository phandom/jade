package smartHOntology;

import jade.content.AgentAction;
import jade.core.AID;

public class LightOff implements AgentAction{

	//who want to turn off light
	 private static final long serialVersionUID = 1L;
	  

	  private AID requester = null;

	  private Light lightToOff = null;
	  
	  
	  public LightOff()
	  {
	    super();
	  }
	  
	  
	  public AID getRequester()
	  {
	    return requester;
	  }
	  public void setRequester(AID requester)
	  {
	    this.requester = requester;
	  }

	  public Light getLightToOff()
	  {
	    return lightToOff;
	  }

	  public void setLightToOff(Light lightToOff)
	  {
	    this.lightToOff = lightToOff;
	  }
}
