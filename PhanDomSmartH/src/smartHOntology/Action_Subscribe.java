package smartHOntology;

import jade.content.AgentAction;

public class Action_Subscribe implements AgentAction {
	
	private Concept_Status status;

	public Concept_Status getStatus() {
		return status;
	}

	public void setStatus(Concept_Status status) {
		this.status = status;
	}
	

}
