package smartHOntology;

import jade.content.Concept;

public class Light implements Concept{
	// include things link ID, name, status
	
	private String deviceID = null;
	
	//in Which Room is the device
	private String location = null; 
	
	//
	private Concept_Status status = null;
	
	public Light() {
		super();
	}
	
	
	public String getDeviceID()
	{
		return deviceID;
	}
	
	public void setDevice( String deviceID) {
		this.deviceID = deviceID;
	}
	
	public String getlocation()
	  {
	    return location;
	  }
	  public void setlocation(String location)
	  {
	    this.location = location;
	  }


	public Concept_Status getStatus() {
		return status;
	}


	public void setStatus(Concept_Status status) {
		this.status = status;
	}


	



}
