package smartHOntology;

import jade.content.Concept;

public class Concept_Status implements Concept {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String status;
	private String grade;
	
	public Concept_Status(){
		super();
	}
	
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public Concept_Status ( String status) {
		super();
		this.setStatus(status);
		
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
}
