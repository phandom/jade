package smartHOntology;

import jade.content.AgentAction;
import jade.core.AID;
import jade.util.leap.ArrayList;

public class Action_SendStatus implements AgentAction {
 
	private AID roomAgent;
	private ArrayList results;
	public AID getRoomAgent() {
		return roomAgent;
	}
	public void setRoomAgent(AID roomAgent) {
		this.roomAgent = roomAgent;
	}
	public ArrayList getResults() {
		return results;
	}
	public void setResults(ArrayList results) {
		this.results = results;
	}
	
	
}
