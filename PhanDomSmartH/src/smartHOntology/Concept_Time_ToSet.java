package smartHOntology;

import jade.content.Concept;

public class Concept_Time_ToSet implements Concept {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private long time;

	
	public Concept_Time_ToSet(){
		super();
	}


	public long getTime() {
		return time;
	}


	public void setTime(long time) {
		this.time = time;
	}
	

	
	
	
	
}
