package Behaviour;

import Protocols.LightOnFromTimer;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

public class TimerOnBehaviour extends TickerBehaviour{

	/**
	 * hier werden die Lichte genau nach eine Woche wiederholt angeschaltet.
	 */
	private static final long serialVersionUID = 1L;

	public TimerOnBehaviour(Agent a, long period) {
		super(a, period);
		// TODO Auto-generated constructor stub
		//1 day = 86400000
	}

	@Override
	protected void onTick() {
		// TODO Auto-generated method stub
		this.myAgent.addBehaviour( new LightOnFromTimer(this.getAgent()) );
	}

	
}
