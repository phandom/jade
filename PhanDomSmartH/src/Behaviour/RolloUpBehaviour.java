package Behaviour;

import Protocols.RolloUpFromTimer;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

public class RolloUpBehaviour extends TickerBehaviour{

	/**
	 * hier wird es nach eine Woche die Rollos hoch gelassen
	 */
	private static final long serialVersionUID = 1L;

	public RolloUpBehaviour(Agent a, long period) {
		super(a, period);
		// TODO Auto-generated constructor stub
		//1 day = 86400000
	}

	@Override
	protected void onTick() {
		// TODO Auto-generated method stub
		this.myAgent.addBehaviour( new RolloUpFromTimer(this.getAgent()) );
	}

	
}
