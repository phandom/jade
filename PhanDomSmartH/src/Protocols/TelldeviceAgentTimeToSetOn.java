package Protocols;

import java.util.Vector;

import agent.UserAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SequentialBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;

public class TelldeviceAgentTimeToSetOn extends SequentialBehaviour {
	   
	  
		/**
		 *  * deviceAgent wird jetzt informiert, 
		 * wann die lichte angeschaltet werden sollen
		 */
		private static final long serialVersionUID = 1L;

		public String Status = null;
		
		private AID deviceAgent = null;
		
		

		public TelldeviceAgentTimeToSetOn(Agent a)
		{
			super(a);
		}

		public void onStart()
		{
			this.addSubBehaviour(new SimpleBehaviour() {
				
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
				@Override
				public void action() {
					// TODO Auto-generated method stub
					 DFAgentDescription template = new DFAgentDescription();
					   ServiceDescription sd = new ServiceDescription();
					   sd.setType( "Light-Status" );
					   template.addServices( sd );
					   try { //in action Methode des SimpleBehaviour
					     DFAgentDescription[] dfds = DFService.search(this.myAgent, template );

					     if( dfds.length > 0 ) 
					     {
					     deviceAgent = dfds[0].getName();
					     System.out.println( "LA found: "+deviceAgent );
					     
					     }
					     }
					     catch( FIPAException fe )
					     {
					     fe.printStackTrace();
					     }
					   
					
				
					
				}
				  @Override
				     public boolean done()
				     {
				       boolean ret = false;
				       if (deviceAgent != null)
				      {
				        ret = true;
				      }
				       return ret;
				       
				     }
			}); //end of SimpleBehaviour
			
			
			ACLMessage m = new ACLMessage(ACLMessage.REQUEST);
			this.addSubBehaviour(
					new AchieveREInitiator(this.myAgent, m){

						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

						@Override
						protected void handleAgree(ACLMessage agree) {
							// TODO Auto-generated method stub
							System.out.println("Agent received" + agree);
						}

						@Override
						protected void handleInform(ACLMessage inform) {
							// TODO Auto-generated method stub
							System.out.println(  inform.getContent());
						
							
						}

						@Override
						protected void handleRefuse(ACLMessage refuse) {
						 System.out.println("received refuse" + refuse);
						 this.reset(new ACLMessage(ACLMessage.REQUEST));
						}

						@Override
						protected Vector prepareRequests(ACLMessage request) {
							// TODO Auto-generated method stub
							Vector v = super.prepareRequests(request);
							for ( Object o: v) {
								ACLMessage m = (ACLMessage) o;
								m.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
								m.addReceiver(deviceAgent);
								int day = 0;
								if(UserAgent.myGui.daysBox.getSelectedItem().equals("Monday")) day=2;
								else if(UserAgent.myGui.daysBox.getSelectedItem().equals("Sunday")) day=1;
								else if(UserAgent.myGui.daysBox.getSelectedItem().equals("Tuesday")) day=3;
								else if(UserAgent.myGui.daysBox.getSelectedItem().equals("Wednesday")) day=4;
								else if(UserAgent.myGui.daysBox.getSelectedItem().equals("Thursday")) day=5;
								else if(UserAgent.myGui.daysBox.getSelectedItem().equals("Friday")) day=6;
								else if(UserAgent.myGui.daysBox.getSelectedItem().equals("Saturday")) day=7;
								
								
								
								m.setContent("TimeOn"+ UserAgent.myGui.deOff.getFormat().format(UserAgent.myGui.sm2.getValue()) + day);
								 

							}
							return v;
						
						}

						
						
					} 
					);
			
		} //End of onStart();

		

		public void reset()
		   {
		   super.reset();
		   this.deviceAgent = null;
		   }

	}
