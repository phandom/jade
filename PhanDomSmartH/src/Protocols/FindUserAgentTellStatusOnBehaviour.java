package Protocols;

import java.util.Vector;

import agent.UserAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SequentialBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;

public class FindUserAgentTellStatusOnBehaviour extends SequentialBehaviour{

	/**
	 * hier wird eine Nachricht an UserAgent geschickt und beinhaltet den status des ausgewählten 
	 * Objects
	 * status : On
	 */
	public AID userAgent;
	
	private static final long serialVersionUID = 1L;

	public FindUserAgentTellStatusOnBehaviour(Agent a)
	{
		super(a);
	}

	protected boolean done = false;

	@Override
	public void onStart() {
		// TODO Auto-generated method stub

		this.addSubBehaviour(new SimpleBehaviour() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void action() {
				// TODO Auto-generated method stub
				 DFAgentDescription template = new DFAgentDescription();
				   ServiceDescription sd = new ServiceDescription();
				   sd.setType( "Answer-Service" );
				   template.addServices( sd );
				   try { //in action Methode des SimpleBehaviour
				     DFAgentDescription[] dfds = DFService.search(this.myAgent, template );

				     if( dfds.length > 0 ) 
				     {
				     userAgent = dfds[0].getName();
				     System.out.println( "LA found: "+userAgent );

				     }
				     }
				     catch( FIPAException fe )
				     {
				     fe.printStackTrace();
				     }
			}
			
			

			  @Override
			     public boolean done()
			     {
			       boolean ret = false;
			       if (userAgent != null)
			      {
			        ret = true;
			      }
			       return ret;
			       
			     }
			
			
		});

		ACLMessage m = new ACLMessage(ACLMessage.REQUEST);
		this.addSubBehaviour(
				new AchieveREInitiator(this.myAgent, m){

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					protected void handleAgree(ACLMessage agree) {
						// TODO Auto-generated method stub
						System.out.println("Agent received" + agree);
					}

					@Override
					protected void handleInform(ACLMessage inform) {
						// TODO Auto-generated method stub
						System.out.println("Request " + inform.getContent());
						
						
					}

					@Override
					protected void handleRefuse(ACLMessage refuse) {
					 System.out.println("received refuse" + refuse);
					 this.reset(new ACLMessage(ACLMessage.REQUEST));
					}

					@Override
					protected Vector prepareRequests(ACLMessage request) {
						// TODO Auto-generated method stub
						Vector v = super.prepareRequests(request);
						for ( Object o: v) {
							ACLMessage m = (ACLMessage) o;
							m.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
							m.addReceiver(userAgent);
						    if (UserAgent.myGui.deviceName.getSelectedItem().equals("light1")) {
							m.setContent("light1on");}
							else if (UserAgent.myGui.deviceName.getSelectedItem().equals("light2")) {
								m.setContent("light2on");}
							else {m.setContent("");}
							}
						
						
						return v;
					
					}

					
					
				} 
				);
	}


}
