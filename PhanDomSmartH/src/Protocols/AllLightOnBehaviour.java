package Protocols;

import java.awt.Color;
import java.util.Vector;

import agent.UserAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;
import jade.proto.states.MsgReceiver;

public class AllLightOnBehaviour extends SequentialBehaviour {

/**
	 *  * timer-Nachricht
	 * Nachricht wird an deviceAgent geschickt.
	 * beinhaltet eine Uhrzeit und den Wochentag, wann die Lichte angeschaltet werden soll.
	 */
	private static final long serialVersionUID = 1L;

public String Status = null;
public static String lightOnMessage = null;
public static String lighOnMessageSunday = null;
public static String lighOnMessageMon = null;
public static String lighOnMessageTue = null;
public static String lighOnMessageWende = null;
public static String lighOnMessageThir = null;
public static String lighOnMessageFri = null;
public static String lighOnMessageSat = null;

	private AID deviceAgent = null;

	public AllLightOnBehaviour(Agent a)
	{
		super(a);
	}

	public void onStart()
	{
		this.addSubBehaviour(new SimpleBehaviour() {
			
			@Override
			public void action() {
				// TODO Auto-generated method stub
				 DFAgentDescription template = new DFAgentDescription();
				   ServiceDescription sd = new ServiceDescription();
				   sd.setType( "Light-Status" );
				   template.addServices( sd );
				   try { //in action Methode des SimpleBehaviour
				     DFAgentDescription[] dfds = DFService.search(this.myAgent, template );

				     if( dfds.length > 0 ) 
				     {
				     deviceAgent = dfds[0].getName();
				     System.out.println( "LA found: "+ deviceAgent );
				     
				     }
				     }
				     catch( FIPAException fe )
				     {
				     fe.printStackTrace();
				     }
			
				
			}
			  @Override
			     public boolean done()
			     {
			       boolean ret = false;
			       if (deviceAgent != null)
			      {
			        ret = true;
			      }
			       return ret;
			       
			     }
		}); //end of SimpleBehaviour
		
		
		ACLMessage m = new ACLMessage(ACLMessage.REQUEST);
		this.addSubBehaviour(
				new AchieveREInitiator(this.myAgent, m){

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					protected void handleAgree(ACLMessage agree) {
						// TODO Auto-generated method stub
						System.out.println("Agent received" + agree);
					}

					@Override
					protected void handleInform(ACLMessage inform) {
						// TODO Auto-generated method stub
						System.out.println(inform.getContent());
						//hier kommt eine Methode um die Lichte anzuschalten
						
					}

					@Override
					protected void handleRefuse(ACLMessage refuse) {
					 System.out.println("received refuse" + refuse);
					 this.reset(new ACLMessage(ACLMessage.REQUEST));
					}

					@Override
					protected Vector prepareRequests(ACLMessage request) {
						// TODO Auto-generated method stub
						Vector v = super.prepareRequests(request);
						for ( Object o: v) {
							ACLMessage m = (ACLMessage) o;
							m.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
							m.addReceiver(deviceAgent);
							UserAgent.myGui.timerArea.setForeground(Color.GREEN);
							if (UserAgent.myGui.daysBox.getSelectedItem().equals("Monday")) {
								lighOnMessageMon = "Lights will be turn on at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(lighOnMessageMon);
								m.setContent("SETON");
							}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Tuesday")) {
								lighOnMessageTue = "Lights will be turn on at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(lighOnMessageTue);
								m.setContent("SETON");
							}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Thursday")) {
								lighOnMessageThir = "Lights will be turn on at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(lighOnMessageThir);
								m.setContent("SETON");
							}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Wednesday")) {
								lighOnMessageWende = "Lights will be turn on at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(lighOnMessageWende);
								m.setContent("SETON");
								}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Friday")) {
								lighOnMessageFri = "Lights will be turn on at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(lighOnMessageFri);
								m.setContent("SETON");
								}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Saturday")) {
								lighOnMessageSat = "Lights will be turn on at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(lighOnMessageSat);
								m.setContent("SETON");
								}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Sunday")) {
								lighOnMessageSunday = "Lights will be turn on at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(lighOnMessageSunday);
								m.setContent("SETON");
								}
							
						
							else {
							lightOnMessage = ("chose a day");
							UserAgent.myGui.timerArea.setText(lightOnMessage);
							m.setContent("SETON");
							}
						}
						return v;					}

					
					
				} 
				);
		
	} //End of onStart();

	

	public void reset()
	   {
	   super.reset();
	   this.deviceAgent = null;
	   }

}
