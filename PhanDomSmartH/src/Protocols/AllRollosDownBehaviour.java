package Protocols;

import java.awt.Color;
import java.util.Vector;

import agent.UserAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;
import jade.proto.states.MsgReceiver;

public class AllRollosDownBehaviour extends SequentialBehaviour {

/**
	 *  * timer-Nachricht
	 * Nachricht wird an deviceAgent geschickt.
	 * beinhaltet eine Uhrzeit und den Wochentag, wann die Rollos runter gesteuert werden soll.
	 */
	private static final long serialVersionUID = 1L;

public String Status = null;
	
	private AID deviceAgent = null;
	public static String RollosOnMessage = null;
	public static String RollosOnMessageSunday = null;
	public static String RollosOnMessageMon = null;
	public static String RollosOnMessageTue = null;
	public static String RollosOnMessageWende = null;
	public static String RollosOnMessageThir = null;
	public static String RollosOnMessageFri = null;
	public static String RollosOnMessageSat = null;

	public AllRollosDownBehaviour(Agent a)
	{
		super(a);
	}

	public void onStart()
	{
		this.addSubBehaviour(new SimpleBehaviour() {
			
			@Override
			public void action() {
				// TODO Auto-generated method stub
				 DFAgentDescription template = new DFAgentDescription();
				   ServiceDescription sd = new ServiceDescription();
				   sd.setType( "Light-Status" );
				   template.addServices( sd );
				   try { //in action Methode des SimpleBehaviour
				     DFAgentDescription[] dfds = DFService.search(this.myAgent, template );

				     if( dfds.length > 0 ) 
				     {
				     deviceAgent = dfds[0].getName();
				     System.out.println( "LA found: "+ deviceAgent );
				     
				     }
				     }
				     catch( FIPAException fe )
				     {
				     fe.printStackTrace();
				     }
			
				
			}
			  @Override
			     public boolean done()
			     {
			       boolean ret = false;
			       if (deviceAgent != null)
			      {
			        ret = true;
			      }
			       return ret;
			       
			     }
		}); //end of SimpleBehaviour
		
		
		ACLMessage m = new ACLMessage(ACLMessage.REQUEST);
		this.addSubBehaviour(
				new AchieveREInitiator(this.myAgent, m){

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					protected void handleAgree(ACLMessage agree) {
						// TODO Auto-generated method stub
						System.out.println("Agent received" + agree);
					}

					@Override
					protected void handleInform(ACLMessage inform) {
						// TODO Auto-generated method stub
						System.out.println(inform.getContent());
						//hier kommt eine Methode um die Lichte anzuschalten
						
					}

					@Override
					protected void handleRefuse(ACLMessage refuse) {
					 System.out.println("received refuse" + refuse);
					 this.reset(new ACLMessage(ACLMessage.REQUEST));
					}

					@Override
					protected Vector prepareRequests(ACLMessage request) {
						// TODO Auto-generated method stub
						Vector v = super.prepareRequests(request);
						for ( Object o: v) {
							ACLMessage m = (ACLMessage) o;
							m.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
							m.addReceiver(deviceAgent);
							UserAgent.myGui.timerArea.setForeground(Color.GREEN);
							if (UserAgent.myGui.daysBox.getSelectedItem().equals("Monday")) {
								RollosOnMessageMon = "Jalousien will be turn down at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(RollosOnMessageMon);
								m.setContent("SETDOWN");
							}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Tuesday")) {
								RollosOnMessageTue = "Jalousien will be turn down at  :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(RollosOnMessageTue);
								m.setContent("SETDOWN");
							}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Thursday")) {
								RollosOnMessageThir = "Jalousien will be turn down at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(RollosOnMessageThir);
								m.setContent("SETDOWN");
							}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Wednesday")) {
								RollosOnMessageWende = "Jalousien will be turn down at  :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(RollosOnMessageWende);
								m.setContent("SETDOWN");
								}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Friday")) {
								RollosOnMessageFri = "Jalousien will be turn down at  :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(RollosOnMessageFri);
								m.setContent("SETDOWN");
								}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Saturday")) {
								RollosOnMessageSat = "Jalousien will be turn down at  :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(RollosOnMessageSat);
								m.setContent("SETDOWN");
								}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Sunday")) {
								RollosOnMessageSunday = "Jalousien will be turn down at  :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(RollosOnMessageSunday);
								m.setContent("SETDOWN");
								}
							
						
							else {
								RollosOnMessage = ("chose a day");
							UserAgent.myGui.timerArea.setText(RollosOnMessage);
							m.setContent("SETDOWN");
							}
						}
						return v;		
					
					}

					
					
				} 
				);
		
	} //End of onStart();

	

	public void reset()
	   {
	   super.reset();
	   this.deviceAgent = null;
	   }

}
