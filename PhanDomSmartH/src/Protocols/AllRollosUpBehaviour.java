package Protocols;

import java.awt.Color;
import java.io.IOException;
import java.util.Vector;

import agent.UserAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;
import jade.proto.states.MsgReceiver;
import smartHOntology.Concept_Time_ToSet;

public class AllRollosUpBehaviour extends SequentialBehaviour {

/** *  * timer-Nachricht
	 * Nachricht wird an deviceAgent geschickt.
	 * beinhaltet eine Uhrzeit und den Wochentag, wann die Rollos hoch gesteuert werden soll.
	 * 
	 */
	private static final long serialVersionUID = 1L;

public String Status = null;

public static String RollosOffMessage = null;
public static String RollosOffMessageSunday = null;
public static String RollosOffMessageMon = null;
public static String RollosOffMessageTue = null;
public static String RollosOffMessageWende = null;
public static String RollosOffMessageThir = null;
public static String RollosOffMessageFri = null;
public static String RollosOffMessageSat = null;
	
	private AID deviceAgent = null;

	public AllRollosUpBehaviour(Agent a)
	{
		super(a);
	}

	public void onStart()
	{
		this.addSubBehaviour(new SimpleBehaviour() {
			
			@Override
			public void action() {
				// TODO Auto-generated method stub
				 DFAgentDescription template = new DFAgentDescription();
				   ServiceDescription sd = new ServiceDescription();
				   sd.setType( "Light-Status" );
				   template.addServices( sd );
				   try { //in action Methode des SimpleBehaviour
				     DFAgentDescription[] dfds = DFService.search(this.myAgent, template );

				     if( dfds.length > 0 ) 
				     {
				     deviceAgent = dfds[0].getName();
				     System.out.println( "LA found: "+ deviceAgent );
				     
				     }
				     }
				     catch( FIPAException fe )
				     {
				     fe.printStackTrace();
				     }
				 
				
			
				
			}
			  @Override
			     public boolean done()
			     {
			       boolean ret = false;
			       if (deviceAgent != null)
			      {
			        ret = true;
			      }
			       return ret;
			       
			     }
		}); //end of SimpleBehaviour
		
		
		ACLMessage m = new ACLMessage(ACLMessage.REQUEST);
		this.addSubBehaviour(
				new AchieveREInitiator(this.myAgent, m){

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					protected void handleAgree(ACLMessage agree) {
						// TODO Auto-generated method stub
						System.out.println("Agent received" + agree);
					}

					@Override
					protected void handleInform(ACLMessage inform) {
						// TODO Auto-generated method stub
						System.out.println(inform.getContent());
						//hier kommt eine Methode um die Lichte anzuschalten
						
					}

					@Override
					protected void handleRefuse(ACLMessage refuse) {
					 System.out.println("received refuse" + refuse);
					 this.reset(new ACLMessage(ACLMessage.REQUEST));
					}

					@Override
					protected Vector prepareRequests(ACLMessage request) {
						// TODO Auto-generated method stub
						Vector v = super.prepareRequests(request);
						for ( Object o: v) {
							ACLMessage m = (ACLMessage) o;
							m.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
							m.addReceiver(deviceAgent);
							UserAgent.myGui.timerArea.setForeground(Color.YELLOW);
							if (UserAgent.myGui.daysBox.getSelectedItem().equals("Monday")) {
								RollosOffMessageMon = "Jalousien will be turn up at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(RollosOffMessageMon);
								m.setContent("SETUP");
							}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Tuesday")) {
								RollosOffMessageTue = "Jalousien will be turn up at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(RollosOffMessageTue);
								m.setContent("SETUP");
							}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Thursday")) {
								RollosOffMessageThir = "Jalousien will be turn up at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(RollosOffMessageThir);
								m.setContent("SETUP");
							}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Wednesday")) {
								RollosOffMessageWende = "Jalousien will be turn up at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(RollosOffMessageWende);
								m.setContent("SETUP");
								}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Friday")) {
								RollosOffMessageFri = "Jalousien will be turn up at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(RollosOffMessageFri);
								m.setContent("SETUP");
								}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Saturday")) {
								RollosOffMessageSat = "Jalousien will be turn up at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(RollosOffMessageSat);
								m.setContent("SETUP");
								}
							else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Sunday")) {
								RollosOffMessageSunday = "Jalousien will be turn up at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
								UserAgent.myGui.timerArea.setText(RollosOffMessageSunday);
								m.setContent("SETUP");
								}
							
						
							else {
								RollosOffMessage = ("chose a day");
							UserAgent.myGui.timerArea.setText(RollosOffMessage);
							m.setContent("SETUP");
							}
						}
						return v;		
//		
//					
					}

					
					
				} 
				);
		
	} //End of onStart();

	

	public void reset()
	   {
	   super.reset();
	   this.deviceAgent = null;
	   }

}
