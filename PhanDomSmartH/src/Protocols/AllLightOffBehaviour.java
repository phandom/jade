package Protocols;

import java.awt.Color;
import java.util.Vector;

import agent.UserAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SequentialBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;

public class AllLightOffBehaviour extends SequentialBehaviour {

	/**
	 * timer-Nachricht
	 * Nachricht wird an deviceAgent geschickt.
	 * beinhaltet eine Uhrzeit und den Wochentag, wann die Lichte ausgeschaltet werden soll.
	 * 
	 *  
	*/
	private static final long serialVersionUID = 1L;

	public String Status = null;
	public static String lightOffMessage = null;
	public static String lighOffMessageSunday = null;
	public static String lighOffMessageMon = null;
	public static String lighOffMessageTue = null;
	public static String lighOffMessageWende = null;
	public static String lighOffMessageThir = null;
	public static String lighOffMessageFri = null;
	public static String lighOffMessageSat = null;

	private AID deviceAgent = null;

	public AllLightOffBehaviour(Agent a) {
		super(a);
	}

	public void onStart() {
		this.addSubBehaviour(new SimpleBehaviour() {

			@Override
			public void action() {
				// TODO Auto-generated method stub
				DFAgentDescription template = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType("Light-Status");
				template.addServices(sd);
				try { // in action Methode des SimpleBehaviour
					DFAgentDescription[] dfds = DFService.search(this.myAgent, template);

					if (dfds.length > 0) {
						deviceAgent = dfds[0].getName();
						System.out.println("LA found: " + deviceAgent);

					}
				} catch (FIPAException fe) {
					fe.printStackTrace();
				}

			}

			@Override
			public boolean done() {
				boolean ret = false;
				if (deviceAgent != null) {
					ret = true;
				}
				return ret;

			}
		}); // end of SimpleBehaviour

		ACLMessage m = new ACLMessage(ACLMessage.REQUEST);
		this.addSubBehaviour(new AchieveREInitiator(this.myAgent, m) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void handleAgree(ACLMessage agree) {
				// TODO Auto-generated method stub
				System.out.println("Agent received" + agree);
			}

			@Override
			protected void handleInform(ACLMessage inform) {
				// TODO Auto-generated method stub
				System.out.println(inform.getContent());
				// hier kommt eine Methode um die Lichte anzuschalten

			}

			@Override
			protected void handleRefuse(ACLMessage refuse) {
				System.out.println("received refuse" + refuse);
				this.reset(new ACLMessage(ACLMessage.REQUEST));
			}

			@Override
			protected Vector prepareRequests(ACLMessage request) {
				// TODO Auto-generated method stub
				Vector v = super.prepareRequests(request);
				for (Object o : v) {
					ACLMessage m = (ACLMessage) o;
					m.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
					m.addReceiver(deviceAgent);
					UserAgent.myGui.timerArea.setForeground(Color.YELLOW);
					if (UserAgent.myGui.daysBox.getSelectedItem().equals("Monday")) {
						lighOffMessageMon = "Lights will be turn off at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
						UserAgent.myGui.timerArea.setText(lighOffMessageMon);
						m.setContent("SETOFF");
					}
					else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Tuesday")) {
						lighOffMessageTue = "Lights will be turn off at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
						UserAgent.myGui.timerArea.setText(lighOffMessageTue);
						m.setContent("SETOFF");
					}
					else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Thursday")) {
						lighOffMessageThir = "Lights will be turn off at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
						UserAgent.myGui.timerArea.setText(lighOffMessageThir);
						m.setContent("SETOFF");
					}
					else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Wednesday")) {
						lighOffMessageWende = "Lights will be turn off at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
						UserAgent.myGui.timerArea.setText(lighOffMessageWende);
						m.setContent("SETOFF");
						}
					else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Friday")) {
						lighOffMessageFri = "Lights will be turn off at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
						UserAgent.myGui.timerArea.setText(lighOffMessageFri);
						m.setContent("SETOFF");
						}
					else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Saturday")) {
						lighOffMessageSat = "Lights will be turn off at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
						UserAgent.myGui.timerArea.setText(lighOffMessageSat);
						m.setContent("SETOFF");
						}
					else if (UserAgent.myGui.daysBox.getSelectedItem().equals("Sunday")) {
						lighOffMessageSunday = "Lights will be turn off at :  " + UserAgent.myGui.de.getFormat().format(UserAgent.myGui.sm.getValue())+"on :   " + UserAgent.myGui.daysBox.getSelectedItem();
						UserAgent.myGui.timerArea.setText(lighOffMessageSunday);
						m.setContent("SETOFF");
						}
					
				
					else {
					lightOffMessage = ("chose a day");
					UserAgent.myGui.timerArea.setText(lightOffMessage);
					m.setContent("SETOFF");
					}
				}
				return v;
				//
				//
			}

		});

	} // End of onStart();

	public void reset() {
		super.reset();
		this.deviceAgent = null;
	}

}
