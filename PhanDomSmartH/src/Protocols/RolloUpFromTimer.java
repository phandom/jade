package Protocols;

import java.io.IOException;
import java.util.Vector;

import agent.UserAgent;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;
import jade.proto.states.MsgReceiver;
import smartHOntology.Concept_Time_ToSet;

public class RolloUpFromTimer extends SequentialBehaviour {

/**
	 * hier werden die Rollos hoch geschaltet (timer-function)
	 */
	private static final long serialVersionUID = 1L;

public String Status = null;
	
	private AID deviceAgent = null;

	public RolloUpFromTimer(Agent a)
	{
		super(a);
	}

	public void onStart()
	{
		this.addSubBehaviour(new SimpleBehaviour() {
			
			@Override
			public void action() {
				// TODO Auto-generated method stub
				 DFAgentDescription template = new DFAgentDescription();
				   ServiceDescription sd = new ServiceDescription();
				   sd.setType( "Light-Status" );
				   template.addServices( sd );
				   try { //in action Methode des SimpleBehaviour
				     DFAgentDescription[] dfds = DFService.search(this.myAgent, template );

				     if( dfds.length > 0 ) 
				     {
				     deviceAgent = dfds[0].getName();
				     System.out.println( "LA found: "+ deviceAgent );
				     
				     }
				     }
				     catch( FIPAException fe )
				     {
				     fe.printStackTrace();
				     }
				 
				
			
				
			}
			  @Override
			     public boolean done()
			     {
			       boolean ret = false;
			       if (deviceAgent != null)
			      {
			        ret = true;
			      }
			       return ret;
			       
			     }
		}); //end of SimpleBehaviour
		
		
		ACLMessage m = new ACLMessage(ACLMessage.REQUEST);
		this.addSubBehaviour(
				new AchieveREInitiator(this.myAgent, m){

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					protected void handleAgree(ACLMessage agree) {
						// TODO Auto-generated method stub
						System.out.println("Agent received" + agree);
					}

					@Override
					protected void handleInform(ACLMessage inform) {
						// TODO Auto-generated method stub
						System.out.println(inform.getContent());
						//hier kommt eine Methode um die Lichte anzuschalten
						
					}

					@Override
					protected void handleRefuse(ACLMessage refuse) {
					 System.out.println("received refuse" + refuse);
					 this.reset(new ACLMessage(ACLMessage.REQUEST));
					}

					@Override
					protected Vector prepareRequests(ACLMessage request) {
						// TODO Auto-generated method stub
						Vector v = super.prepareRequests(request);
						for ( Object o: v) {
							ACLMessage m = (ACLMessage) o;
							m.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
							m.addReceiver(deviceAgent);
							m.setContent("UP");
						
							
						}
						return v;
//		
//					
					}

					
					
				} 
				);
		
	} //End of onStart();

	

	public void reset()
	   {
	   super.reset();
	   this.deviceAgent = null;
	   }

}
