package Protocols;

import java.util.Vector;

import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.domain.FIPANames;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;
import jade.util.leap.ArrayList;
import smartHOntology.Action_SendStatus;
import smartHOntology.RoomOntology;

public class SendStatustoRoomAgent extends AchieveREInitiator {
	
	ArrayList results;
	
	
	
	public SendStatustoRoomAgent(Agent a, ACLMessage msg, ArrayList result) {
		super(a, msg);
		this.results = results;
	}

	private Codec codec = new SLCodec();
	
	//send Ergebnisse an roomAgent
	protected Vector prepareRequest(ACLMessage request)
	{
		Vector v = new Vector ();
		
		request.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
		request.setOntology(RoomOntology.getInstance().getName());
		request.setLanguage(FIPANames.ContentLanguage.FIPA_SL);
		request.setConversationId("status-UserAgent-RoomAgent");
		
		Action_SendStatus extendResults = new Action_SendStatus();
		extendResults.setResults(results);
		
		
		Action a = new Action();
		a.setActor(myAgent.getAID());
		a.setAction(extendResults);
		
		
		try{
			
			myAgent.getContentManager().registerLanguage(codec, FIPANames.ContentLanguage.FIPA_SL);
			myAgent.getContentManager().registerOntology(RoomOntology.getInstance());
			myAgent.getContentManager().fillContent(request, a);
			
			v.add(request);
			
		}
		catch(CodecException e){
		e.printStackTrace();
		}
		catch(OntologyException e){
			e.printStackTrace();
			}
		return v;
		
	}


	//roomAgent hat status bekommen
	
	@Override
	protected void handleAgree(ACLMessage agree) {
		// TODO Auto-generated method stub
		super.handleAgree(agree);
		System.out.println("got Agree from roomAgent"+ agree.getContent());
	}

	@Override
	protected void handleRefuse(ACLMessage refuse) {
		// TODO Auto-generated method stub
		super.handleRefuse(refuse);
		System.out.println("got Refuse from roomAgent"+ refuse.getContent());
	}
		
	@Override
	protected void handleNotUnderstood(ACLMessage notUnderStood) {
		// TODO Auto-generated method stub
		super.handleRefuse(notUnderStood);
		System.out.println("got NOTUNDERSTOOD from roomAgent"+ notUnderStood.getContent());
	}
		

	
}
